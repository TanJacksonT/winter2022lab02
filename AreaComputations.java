public class AreaComputations {
	
	public static double areaSquare(double lengthOfSquare){
	
		return lengthOfSquare * lengthOfSquare;
	
	}
	
	public double areaRectangle(double lengthOfRectangle, double widthOfRectangle){
		
		return lengthOfRectangle * widthOfRectangle;
	
	}
}

		