public class MethodsTest{
	public static void main(String []args){
		
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		
		methodTwoInputNoReturn(x,10.5);
	
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		System.out.println(sumSquareRoot(6,3));
		
		String s1 ="hello";
		String s2 ="goodbye";
		
		System.out.println(s1.length());
		System.out.println(s2.length());
	
		System.out.println(SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn(){
		
		int x = 50; 
		System.out.println("I'm a method that takes no input and returns nothing");
	}
	
	public static void methodOneInputNoReturn(int num){
		
		System.out.println("Inside the method one input no return"); 
		System.out.println(num);
	}
	
	public static void methodTwoInputNoReturn(int num,double num2){
		
		System.out.println(num + num2);
	}
	
	public static int methodNoInputReturnInt(){
		
		return 6;
	}
	public static double sumSquareRoot(int num1, int num2){
		 
		 double total = num1 + num2;
				total = Math.sqrt(total);
				
				return total;
	}
}