import java.util.Scanner;

public class PartThree{
	
	public static void main(String []args){	
		Scanner obj = new Scanner(System.in);
		double inputAreaSquare = obj.nextDouble();
		double firstInputAreaRectangle = obj.nextDouble();
		double secondInputAreaRectangle = obj.nextDouble();
		
		System.out.println("The area of the square is: ");
		System.out.println(AreaComputations.areaSquare(inputAreaSquare));
		
		System.out.println("The area of the rectangle is: ");
		
		AreaComputations sc = new AreaComputations();
		System.out.println(sc.areaRectangle(firstInputAreaRectangle,secondInputAreaRectangle));
		
	}
}